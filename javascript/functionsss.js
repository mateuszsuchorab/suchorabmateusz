(function ($) {


    wow = new WOW(
        {})
        .init();

    function repairNavBar() {
        $('.navbar-nav li').removeClass("active");

        var scrollTop = $(window).scrollTop();
        var homeHeight = $('#home').height() * 0.8;
        var aboutHeight = homeHeight + $('#about').height() * 0.8;
        var projectsHeight = aboutHeight + $('#projects').height() * 0.8;
        var contactHeight = projectsHeight + $('#contact').height();
        if (0 <= scrollTop && scrollTop < homeHeight) {
            $('#LiHome').toggleClass("active");


        }

        if (homeHeight < scrollTop && scrollTop < aboutHeight) {
            $('#LiAbout').toggleClass("active");

        }
        if (aboutHeight < scrollTop && scrollTop < projectsHeight) {
            $('#LiProjects').toggleClass("active");

        }
        if (projectsHeight < scrollTop && scrollTop < contactHeight) {
            $('#LiContact').toggleClass("active");

        }
    }

    $(document).ready(function () {
        repairNavBar();
    });

    $(window).scroll(function () {
        clearTimeout($.data(this, 'scrollTimer'));
        $.data(this, 'scrollTimer', setTimeout(function () {
            repairNavBar();
        }, 250));
    });

    $('.bookmarks').bind('click', function (event) {
        repairNavBar();
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollup').fadeIn();
        } else {
            $('.scrollup').fadeOut();
        }
    });

    $('.scrollup').click(function () {
        $("html, body").animate({scrollTop: 0}, 1000);
        return false;
    });


    //jQuery for page scrolling feature - requires jQuery Easing plugin

    $('.navbar-nav li a, .homeButton').bind('click', function (event) {
        $('.navbar-nav li').removeClass("active");
        var $anchor = $(this);
        var nav = $($anchor.attr('href'));
        if (nav.length) {
            $('html, body').stop().animate({
                scrollTop: $($anchor.attr('href')).offset().top
            }, 1500, 'easeInOutExpo');

            event.preventDefault();
        }

        if (this.className == "homeButton") {
            $('#LiAbout').toggleClass('active');
        }
    });


    $('.navbar-nav li').bind('click', function (event) {
        var $anchor = $(this);
        $anchor.toggleClass('active');
    });

    $('#topBigMail').hover(function() {
        $('#topMailText').css('color', '#da0000');
        $('#topMail').css({'background': 'url(../images/email-5-64-1.png) no-repeat',
            'background-size': 'cover'});
    }, function() {
        $('#topMailText').css('color', '#FFFFFF');
        $('#topMail').css({'background': 'url(../images/email-5-64.png) no-repeat',
            'background-size': 'cover'});
    });

    $('#topBigPhone').hover(function() {
        $('#topPhoneText').css('color', '#da0000');
        $('#topPhone').css({'background': 'url(../images/phone-5-64-1.png) no-repeat',
            'background-size': 'cover'});
    }, function() {
        $('#topPhoneText').css('color', '#FFFFFF');
        $('#topPhone').css({'background': 'url(../images/phone-5-64.png) no-repeat',
            'background-size': 'cover'});
    });


})(jQuery);


